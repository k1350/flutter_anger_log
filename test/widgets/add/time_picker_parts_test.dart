import 'package:anger_log/models/models.dart';
import 'package:anger_log/notifiers/add_page_notifier.dart';
import 'package:anger_log/widgets/add/time_picker_parts.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

final addPageProvider =
    StateNotifierProvider<AddPageStateNotifier, AddPageState>(
        (ref) => AddPageStateNotifier());

void main() {
  /// テスト用の [child] を [MaterialApp] でラップして返す。
  Widget createWidgetForTesting({required Widget child}) {
    return ProviderScope(
        child: MaterialApp(localizationsDelegates: const [
      GlobalMaterialLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
    ], supportedLocales: const [
      Locale('ja', 'JP'),
    ], home: Scaffold(body: child)));
  }

  testWidgets('初期値が表示されている', (tester) async {
    final now = DateTime.now();
    await tester.pumpWidget(createWidgetForTesting(
      child: const TimePickerParts(),
    ));
    await tester.pumpAndSettle();
    final formatter = DateFormat('H:mm', 'ja_JP');
    expect(find.text(formatter.format(now)), findsOneWidget);
  });

  testWidgets('時刻を選択できる', (tester) async {
    await tester.pumpWidget(createWidgetForTesting(
      child: const TimePickerParts(),
    ));

    await tester.pumpAndSettle();
    await tester.tap(find.text('時刻選択'));
    await tester.pumpAndSettle();

    final center = tester
        .getCenter(find.byKey(const ValueKey<String>('time-picker-dial')));
    await tester.tapAt(Offset(center.dx + 50.0, center.dy)); // 6:00
    await tester.pumpAndSettle();
    await tester.tapAt(Offset(center.dx, center.dy + 50.0)); // 30 mins
    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();

    expect(find.text('6:30'), findsOneWidget);
  });
}
