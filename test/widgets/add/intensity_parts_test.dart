import 'package:anger_log/models/models.dart';
import 'package:anger_log/notifiers/add_page_notifier.dart';
import 'package:anger_log/widgets/add/intensity_parts.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';

final addPageProvider =
    StateNotifierProvider<AddPageStateNotifier, AddPageState>(
        (ref) => AddPageStateNotifier());

void main() {
  /// テスト用の [child] を [MaterialApp] でラップして返す。
  Widget createWidgetForTesting({required Widget child}) {
    return ProviderScope(
        child: MaterialApp(localizationsDelegates: const [
      GlobalMaterialLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
    ], supportedLocales: const [
      Locale('ja', 'JP'),
    ], home: Scaffold(body: child)));
  }

  testWidgets('プレースホルダが「感情の強さ」である', (tester) async {
    await tester
        .pumpWidget(createWidgetForTesting(child: const IntensityParts()));
    await tester.pumpAndSettle();
    expect(find.text('感情の強さ'), findsOneWidget);
  });

  testWidgets('初期値が 5 である', (tester) async {
    await tester
        .pumpWidget(createWidgetForTesting(child: const IntensityParts()));
    await tester.pumpAndSettle();
    expect(find.text('5'), findsOneWidget);
  });

  testWidgets('intensity を入力できる', (tester) async {
    await tester
        .pumpWidget(createWidgetForTesting(child: const IntensityParts()));
    await tester.pumpAndSettle();
    await tester.tap(find.text('5'));
    await tester.pumpAndSettle();
    // 仮に key を指定しても widget が 2 つ見つかってしまう
    await tester.tap(find.text('10').last);
    await tester.pumpAndSettle();
    expect(find.text('10'), findsOneWidget);
  });
}
