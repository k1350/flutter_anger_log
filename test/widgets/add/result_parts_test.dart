import 'package:anger_log/models/models.dart';
import 'package:anger_log/notifiers/add_page_notifier.dart';
import 'package:anger_log/widgets/add/result_parts.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';

final addPageProvider =
    StateNotifierProvider<AddPageStateNotifier, AddPageState>(
        (ref) => AddPageStateNotifier());

void main() {
  /// テスト用の [child] を [MaterialApp] でラップして返す。
  Widget createWidgetForTesting({required Widget child}) {
    return ProviderScope(
        child: MaterialApp(localizationsDelegates: const [
      GlobalMaterialLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
    ], supportedLocales: const [
      Locale('ja', 'JP'),
    ], home: Scaffold(body: child)));
  }

  testWidgets('プレースホルダが「結果」である', (tester) async {
    await tester.pumpWidget(createWidgetForTesting(child: const ResultParts()));
    await tester.pumpAndSettle();
    expect(find.text('結果'), findsOneWidget);
  });

  testWidgets('result を入力できる', (tester) async {
    await tester.pumpWidget(createWidgetForTesting(child: const ResultParts()));
    await tester.pumpAndSettle();
    await tester.enterText(find.byType(TextFormField), 'hi');
    expect(find.text('hi'), findsOneWidget);
  });
}
