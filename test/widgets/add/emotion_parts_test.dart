import 'package:anger_log/models/models.dart';
import 'package:anger_log/notifiers/add_page_notifier.dart';
import 'package:anger_log/widgets/add/emotion_parts.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';

final addPageProvider =
    StateNotifierProvider<AddPageStateNotifier, AddPageState>(
        (ref) => AddPageStateNotifier());

void main() {
  /// テスト用の [child] を [MaterialApp] でラップして返す。
  Widget createWidgetForTesting({required Widget child}) {
    return ProviderScope(
        child: MaterialApp(localizationsDelegates: const [
      GlobalMaterialLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
    ], supportedLocales: const [
      Locale('ja', 'JP'),
    ], home: Scaffold(body: child)));
  }

  testWidgets('プレースホルダが「感情」である', (tester) async {
    await tester.pumpWidget(createWidgetForTesting(child: const EmotionParts()));
    await tester.pumpAndSettle();
    expect(find.text('感情'), findsOneWidget);
  });

  testWidgets('emotion を入力できる', (tester) async {
    await tester.pumpWidget(createWidgetForTesting(child: const EmotionParts()));
    await tester.pumpAndSettle();
    await tester.enterText(find.byType(TextFormField), 'hi');
    expect(find.text('hi'), findsOneWidget);
  });
}
