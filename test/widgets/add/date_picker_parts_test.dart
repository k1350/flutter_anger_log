import 'package:anger_log/models/models.dart';
import 'package:anger_log/notifiers/add_page_notifier.dart';
import 'package:anger_log/widgets/add/date_picker_parts.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

final addPageProvider =
    StateNotifierProvider<AddPageStateNotifier, AddPageState>(
        (ref) => AddPageStateNotifier());

void main() {
  /// テスト用の [child] を [MaterialApp] でラップして返す。
  Widget createWidgetForTesting({required Widget child}) {
    return ProviderScope(
        child: MaterialApp(localizationsDelegates: const [
      GlobalMaterialLocalizations.delegate,
      GlobalCupertinoLocalizations.delegate,
      GlobalWidgetsLocalizations.delegate,
    ], supportedLocales: const [
      Locale('ja', 'JP'),
    ], home: Scaffold(body: child)));
  }

  testWidgets('初期値が表示されている', (tester) async {
    final now = DateTime.now();
    await tester.pumpWidget(createWidgetForTesting(
      child: DatePickerParts(firstDate: DateTime(2022), lastDate: now),
    ));
    await tester.pumpAndSettle();
    final formatter = DateFormat('yyyy/MM/dd(E)', 'ja_JP');
    expect(find.text(formatter.format(now)), findsOneWidget);
  });

  testWidgets('日付を選択できる', (tester) async {
    final now = DateTime.now();
    DateTime yesterday = now.subtract(const Duration(days: 1));

    await tester.pumpWidget(createWidgetForTesting(
      child: DatePickerParts(firstDate: DateTime(2022), lastDate: now),
    ));

    await tester.pumpAndSettle();
    await tester.tap(find.text('日付選択'));
    await tester.pumpAndSettle();

    try {
      await tester.tap(find.text(yesterday.day.toString()));
    } catch (_) {
      // 月初日
      yesterday = yesterday.add(const Duration(days: 1));
      await tester.tap(find.text(yesterday.day.toString()));
    }

    await tester.tap(find.text('OK'));
    await tester.pumpAndSettle();

    final formatter = DateFormat('yyyy/MM/dd(E)', 'ja_JP');
    expect(find.text(formatter.format(yesterday)), findsOneWidget);
  });
}
