import 'dart:async';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:synchronized/synchronized.dart';

class DatabaseProvider {
  static DatabaseProvider? _dbProvider;
  static Database? _database;
  final String databaseName = "anger_log.db";
  final String tableName = "anger_log";
  final int version = 1;
  final _lock = Lock();

  DatabaseProvider._internal();

  factory DatabaseProvider() {
    _dbProvider ??= DatabaseProvider._internal();
    return _dbProvider!;
  }

  Future<Database> get database async {
    if (_database == null) {
      await _lock.synchronized(() async {
        // Check again once entering the synchronized block
        _database ??= await createDatabase();
      });
    }
    return _database!;
  }

  /// DB 作成
  Future<Database> createDatabase() async {
    final path = await getDbPath();

    Database db = await openDatabase(
      path,
      version: version,
      onCreate: initDB,
      onUpgrade: upgradeDB,
      onDowngrade: onDatabaseDowngradeDelete,
    );
    return db;
  }

  /// DB のファイルパスを Android と iOS で分けて取得する
  Future<String> getDbPath() async {
    var dbFilePath = '';

    if (Platform.isAndroid) {
      // Androidであれば「getDatabasesPath」を利用
      dbFilePath = await getDatabasesPath();
    } else if (Platform.isIOS) {
      // iOSであれば「getLibraryDirectory」を利用
      final dbDirectory = await getLibraryDirectory();
      dbFilePath = dbDirectory.path;
    } else {
      throw Exception('Unable to determine platform.');
    }
    // ディレクトリの存在を確認する。
    try {
      await Directory(dbFilePath).create(recursive: true);
    } catch (e) {
      rethrow;
    }
    // 配置場所のパスを作成して返却
    final path = join(dbFilePath, databaseName);
    return path;
  }

  /// [version] バージョンのDB作成処理。
  void initDB(Database db, int version) => db.execute(
        """
          CREATE TABLE $tableName(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            date TEXT,
            time TEXT,
            event TEXT,
            thoughts TEXT,
            emotion TEXT,
            intensity int,
            action TEXT,
            result TEXT,
            created TEXT,
            updated TEXT
          )
        """,
      );

  /// DBのアップグレード処理。今はまだ何もしない。
  Future<void> upgradeDB(Database db, int oldVersion, int newVersion) async {
    print("onUpgrade called. oldVersion: $oldVersion, newVersion: $newVersion");
  }
}
