import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/common/constant.dart';
import 'package:anger_log/models/models.dart';
import 'package:anger_log/repository/repository.dart';

class AngerLogListNotifier extends StateNotifier<AngerLogList> {
  AngerLogListNotifier()
      : super(AngerLogList(
            hasData: false,
            hasNext: true,
            isFetchingNext: false,
            offset: 0,
            list: []));

  void setIsFetchingNext(bool isFetchingNext) {
    state = state.copyWith(isFetchingNext: true);
  }

  /// 前回の続きから一覧取得
  Future<void> get() async {
    setIsFetchingNext(true);
    final repository = AngerLogRepository();
    final list =
        await repository.getAngerLogs(Constant.limit, state.offset).catchError(
      (dynamic err) {
        throw err;
      },
    );
    final offset = state.offset + Constant.limit;
    final newList = List<AngerLog>.from(state.list)..addAll(list);
    state = AngerLogList(
        hasData: true,
        hasNext: list.length >= Constant.limit,
        isFetchingNext: false,
        offset: offset,
        list: newList);
  }

  /// 最初から一覧取得
  Future<void> refresh() async {
    setIsFetchingNext(true);
    final repository = AngerLogRepository();
    final list = await repository.getAngerLogs(Constant.limit, 0).catchError(
      (dynamic err) {
        throw err;
      },
    );
    state = AngerLogList(
        hasData: true,
        hasNext: list.length >= Constant.limit,
        isFetchingNext: false,
        offset: Constant.limit,
        list: list);
  }
}
