import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/common/db_date_time.dart';
import 'package:anger_log/models/models.dart';
import 'package:anger_log/repository/repository.dart';

class AddPageStateNotifier extends StateNotifier<AddPageState> {
  AddPageStateNotifier()
      : super(AddPageState(
            date: DateTime.now(),
            time: TimeOfDay.now(),
            event: '',
            thoughts: '',
            emotion: '',
            intensity: 5,
            action: '',
            result: ''));

  void setDate(DateTime date) {
    state = state.copyWith(date: date);
  }

  void setTime(TimeOfDay time) {
    state = state.copyWith(time: time);
  }

  void setEvent(String event) {
    state = state.copyWith(event: event);
  }

  void setThoughts(String thoughts) {
    state = state.copyWith(thoughts: thoughts);
  }

  void setEmotion(String emotion) {
    state = state.copyWith(emotion: emotion);
  }

  void setIntensity(int intensity) {
    state = state.copyWith(intensity: intensity);
  }

  void setAction(String action) {
    state = state.copyWith(action: action);
  }

  void setResult(String result) {
    state = state.copyWith(result: result);
  }

  void setInitialState(
      {int? id,
      required DateTime date,
      required TimeOfDay time,
      required String event,
      required String thoughts,
      required String emotion,
      required int intensity,
      required String action,
      required String result,
      String? created,
      String? updated}) {
    state = AddPageState(
        id: id,
        date: date,
        time: time,
        event: event,
        thoughts: thoughts,
        emotion: emotion,
        intensity: intensity,
        action: action,
        result: result,
        created: created,
        updated: updated);
  }

  Future<void> save() async {
    final utcDateTime = DbDateTime.getDbDateAndTime(state.date, state.time);
    final now = DateTime.now().toUtc().toIso8601String();

    final angerLog = AngerLog(
        date: utcDateTime.date,
        time: utcDateTime.time,
        event: state.event,
        thoughts: state.thoughts,
        emotion: state.emotion,
        intensity: state.intensity,
        action: state.action,
        result: state.result,
        created: now,
        updated: now);

    final repository = AngerLogRepository();
    await repository.insertReminder(angerLog).catchError(
      (dynamic err) {
        throw err;
      },
    );
  }

  Future<void> update() async {
    final utcDateTime = DbDateTime.getDbDateAndTime(state.date, state.time);
    final now = DateTime.now().toUtc().toIso8601String();

    final angerLog = AngerLog(
        id: state.id,
        date: utcDateTime.date,
        time: utcDateTime.time,
        event: state.event,
        thoughts: state.thoughts,
        emotion: state.emotion,
        intensity: state.intensity,
        action: state.action,
        result: state.result,
        created: state.created!,
        updated: now);

    final repository = AngerLogRepository();
    await repository.updateReminder(angerLog).catchError(
      (dynamic err) {
        throw err;
      },
    );
  }

  Future<void> delete() async {
    final repository = AngerLogRepository();
    await repository.deleteAngerLog(state.id!).catchError(
      (dynamic err) {
        throw err;
      },
    );
  }
}
