import 'package:anger_log/dao/dao.dart';
import 'package:anger_log/models/models.dart';

class AngerLogRepository {
  /// [AngerLogDao] を通じてDBにアクセスする。
  final angerLogDao = AngerLogDao();

  /// [angerLog] をDBに保存し、保存したレコードのIDを返す。
  Future insertReminder(AngerLog angerLog) =>
      angerLogDao.createAngerLog(angerLog).catchError(
        (dynamic err) {
          throw err;
        },
      );

  /// [List<AngerLog>] を [limit] 件ずつ取得して返す。
  /// [offset] の位置の次から取得開始する。
  Future getAngerLogs(int limit, int offset) =>
      angerLogDao.getAngerLogs(limit, offset).catchError(
        (dynamic err) {
          throw err;
        },
      );

  /// DB に保存されている [angerLog] を更新し、更新された行数を返す。
  Future updateReminder(AngerLog angerLog) =>
      angerLogDao.updateAngerLog(angerLog).catchError(
        (dynamic err) {
          throw err;
        },
      );

  /// [id] の [angerLog] を DB から消去する。消去された行数を返す。
  Future deleteAngerLog(int id) => angerLogDao.deleteAngerLog(id).catchError(
        (dynamic err) {
          throw err;
        },
      );
}
