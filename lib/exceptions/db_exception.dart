import 'dart:io';

class DBException implements IOException {
  final String message;
  const DBException(this.message);
}
