import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:flutter/foundation.dart';

part 'models.freezed.dart';
part 'models.g.dart';

@freezed
class AddPageState with _$AddPageState {
  const factory AddPageState({
    int? id,
    required DateTime date,
    required TimeOfDay time,
    required String event,
    required String thoughts,
    required String emotion,
    required int intensity,
    required String action,
    required String result,
    String? created,
    String? updated,
  }) = _AddPageState;
}

@freezed
class AngerLog with _$AngerLog {
  factory AngerLog(
      {int? id,
      required String date,
      required String time,
      String? event,
      String? thoughts,
      String? emotion,
      required int intensity,
      String? action,
      String? result,
      required String
          created, // 作成日時。ISO8601文字列表現（例：2019-04-30T01:48:27.701406Z）。
      required String
          updated // 更新日時。ISO8601文字列表現（例：2019-04-30T01:48:27.701406Z）。
      }) = _AngerLog;

  factory AngerLog.fromJson(Map<String, dynamic> json) =>
      _$AngerLogFromJson(json);
}

@freezed
class AngerLogList with _$AngerLogList {
  factory AngerLogList(
      {required bool hasData,
      required bool hasNext,
      required bool isFetchingNext,
      required int offset,
      required List<AngerLog> list}) = _AngerLogList;
}
