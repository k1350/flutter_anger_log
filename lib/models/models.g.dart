// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'models.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_AngerLog _$$_AngerLogFromJson(Map<String, dynamic> json) => _$_AngerLog(
      id: json['id'] as int?,
      date: json['date'] as String,
      time: json['time'] as String,
      event: json['event'] as String?,
      thoughts: json['thoughts'] as String?,
      emotion: json['emotion'] as String?,
      intensity: json['intensity'] as int,
      action: json['action'] as String?,
      result: json['result'] as String?,
      created: json['created'] as String,
      updated: json['updated'] as String,
    );

Map<String, dynamic> _$$_AngerLogToJson(_$_AngerLog instance) =>
    <String, dynamic>{
      'id': instance.id,
      'date': instance.date,
      'time': instance.time,
      'event': instance.event,
      'thoughts': instance.thoughts,
      'emotion': instance.emotion,
      'intensity': instance.intensity,
      'action': instance.action,
      'result': instance.result,
      'created': instance.created,
      'updated': instance.updated,
    };
