// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'models.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AddPageState {
  int? get id => throw _privateConstructorUsedError;
  DateTime get date => throw _privateConstructorUsedError;
  TimeOfDay get time => throw _privateConstructorUsedError;
  String get event => throw _privateConstructorUsedError;
  String get thoughts => throw _privateConstructorUsedError;
  String get emotion => throw _privateConstructorUsedError;
  int get intensity => throw _privateConstructorUsedError;
  String get action => throw _privateConstructorUsedError;
  String get result => throw _privateConstructorUsedError;
  String? get created => throw _privateConstructorUsedError;
  String? get updated => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AddPageStateCopyWith<AddPageState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AddPageStateCopyWith<$Res> {
  factory $AddPageStateCopyWith(
          AddPageState value, $Res Function(AddPageState) then) =
      _$AddPageStateCopyWithImpl<$Res>;
  $Res call(
      {int? id,
      DateTime date,
      TimeOfDay time,
      String event,
      String thoughts,
      String emotion,
      int intensity,
      String action,
      String result,
      String? created,
      String? updated});
}

/// @nodoc
class _$AddPageStateCopyWithImpl<$Res> implements $AddPageStateCopyWith<$Res> {
  _$AddPageStateCopyWithImpl(this._value, this._then);

  final AddPageState _value;
  // ignore: unused_field
  final $Res Function(AddPageState) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? date = freezed,
    Object? time = freezed,
    Object? event = freezed,
    Object? thoughts = freezed,
    Object? emotion = freezed,
    Object? intensity = freezed,
    Object? action = freezed,
    Object? result = freezed,
    Object? created = freezed,
    Object? updated = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      time: time == freezed
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as TimeOfDay,
      event: event == freezed
          ? _value.event
          : event // ignore: cast_nullable_to_non_nullable
              as String,
      thoughts: thoughts == freezed
          ? _value.thoughts
          : thoughts // ignore: cast_nullable_to_non_nullable
              as String,
      emotion: emotion == freezed
          ? _value.emotion
          : emotion // ignore: cast_nullable_to_non_nullable
              as String,
      intensity: intensity == freezed
          ? _value.intensity
          : intensity // ignore: cast_nullable_to_non_nullable
              as int,
      action: action == freezed
          ? _value.action
          : action // ignore: cast_nullable_to_non_nullable
              as String,
      result: result == freezed
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as String,
      created: created == freezed
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as String?,
      updated: updated == freezed
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
abstract class _$$_AddPageStateCopyWith<$Res>
    implements $AddPageStateCopyWith<$Res> {
  factory _$$_AddPageStateCopyWith(
          _$_AddPageState value, $Res Function(_$_AddPageState) then) =
      __$$_AddPageStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {int? id,
      DateTime date,
      TimeOfDay time,
      String event,
      String thoughts,
      String emotion,
      int intensity,
      String action,
      String result,
      String? created,
      String? updated});
}

/// @nodoc
class __$$_AddPageStateCopyWithImpl<$Res>
    extends _$AddPageStateCopyWithImpl<$Res>
    implements _$$_AddPageStateCopyWith<$Res> {
  __$$_AddPageStateCopyWithImpl(
      _$_AddPageState _value, $Res Function(_$_AddPageState) _then)
      : super(_value, (v) => _then(v as _$_AddPageState));

  @override
  _$_AddPageState get _value => super._value as _$_AddPageState;

  @override
  $Res call({
    Object? id = freezed,
    Object? date = freezed,
    Object? time = freezed,
    Object? event = freezed,
    Object? thoughts = freezed,
    Object? emotion = freezed,
    Object? intensity = freezed,
    Object? action = freezed,
    Object? result = freezed,
    Object? created = freezed,
    Object? updated = freezed,
  }) {
    return _then(_$_AddPageState(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
      time: time == freezed
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as TimeOfDay,
      event: event == freezed
          ? _value.event
          : event // ignore: cast_nullable_to_non_nullable
              as String,
      thoughts: thoughts == freezed
          ? _value.thoughts
          : thoughts // ignore: cast_nullable_to_non_nullable
              as String,
      emotion: emotion == freezed
          ? _value.emotion
          : emotion // ignore: cast_nullable_to_non_nullable
              as String,
      intensity: intensity == freezed
          ? _value.intensity
          : intensity // ignore: cast_nullable_to_non_nullable
              as int,
      action: action == freezed
          ? _value.action
          : action // ignore: cast_nullable_to_non_nullable
              as String,
      result: result == freezed
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as String,
      created: created == freezed
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as String?,
      updated: updated == freezed
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$_AddPageState with DiagnosticableTreeMixin implements _AddPageState {
  const _$_AddPageState(
      {this.id,
      required this.date,
      required this.time,
      required this.event,
      required this.thoughts,
      required this.emotion,
      required this.intensity,
      required this.action,
      required this.result,
      this.created,
      this.updated});

  @override
  final int? id;
  @override
  final DateTime date;
  @override
  final TimeOfDay time;
  @override
  final String event;
  @override
  final String thoughts;
  @override
  final String emotion;
  @override
  final int intensity;
  @override
  final String action;
  @override
  final String result;
  @override
  final String? created;
  @override
  final String? updated;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AddPageState(id: $id, date: $date, time: $time, event: $event, thoughts: $thoughts, emotion: $emotion, intensity: $intensity, action: $action, result: $result, created: $created, updated: $updated)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'AddPageState'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('date', date))
      ..add(DiagnosticsProperty('time', time))
      ..add(DiagnosticsProperty('event', event))
      ..add(DiagnosticsProperty('thoughts', thoughts))
      ..add(DiagnosticsProperty('emotion', emotion))
      ..add(DiagnosticsProperty('intensity', intensity))
      ..add(DiagnosticsProperty('action', action))
      ..add(DiagnosticsProperty('result', result))
      ..add(DiagnosticsProperty('created', created))
      ..add(DiagnosticsProperty('updated', updated));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AddPageState &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.date, date) &&
            const DeepCollectionEquality().equals(other.time, time) &&
            const DeepCollectionEquality().equals(other.event, event) &&
            const DeepCollectionEquality().equals(other.thoughts, thoughts) &&
            const DeepCollectionEquality().equals(other.emotion, emotion) &&
            const DeepCollectionEquality().equals(other.intensity, intensity) &&
            const DeepCollectionEquality().equals(other.action, action) &&
            const DeepCollectionEquality().equals(other.result, result) &&
            const DeepCollectionEquality().equals(other.created, created) &&
            const DeepCollectionEquality().equals(other.updated, updated));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(date),
      const DeepCollectionEquality().hash(time),
      const DeepCollectionEquality().hash(event),
      const DeepCollectionEquality().hash(thoughts),
      const DeepCollectionEquality().hash(emotion),
      const DeepCollectionEquality().hash(intensity),
      const DeepCollectionEquality().hash(action),
      const DeepCollectionEquality().hash(result),
      const DeepCollectionEquality().hash(created),
      const DeepCollectionEquality().hash(updated));

  @JsonKey(ignore: true)
  @override
  _$$_AddPageStateCopyWith<_$_AddPageState> get copyWith =>
      __$$_AddPageStateCopyWithImpl<_$_AddPageState>(this, _$identity);
}

abstract class _AddPageState implements AddPageState {
  const factory _AddPageState(
      {final int? id,
      required final DateTime date,
      required final TimeOfDay time,
      required final String event,
      required final String thoughts,
      required final String emotion,
      required final int intensity,
      required final String action,
      required final String result,
      final String? created,
      final String? updated}) = _$_AddPageState;

  @override
  int? get id;
  @override
  DateTime get date;
  @override
  TimeOfDay get time;
  @override
  String get event;
  @override
  String get thoughts;
  @override
  String get emotion;
  @override
  int get intensity;
  @override
  String get action;
  @override
  String get result;
  @override
  String? get created;
  @override
  String? get updated;
  @override
  @JsonKey(ignore: true)
  _$$_AddPageStateCopyWith<_$_AddPageState> get copyWith =>
      throw _privateConstructorUsedError;
}

AngerLog _$AngerLogFromJson(Map<String, dynamic> json) {
  return _AngerLog.fromJson(json);
}

/// @nodoc
mixin _$AngerLog {
  int? get id => throw _privateConstructorUsedError;
  String get date => throw _privateConstructorUsedError;
  String get time => throw _privateConstructorUsedError;
  String? get event => throw _privateConstructorUsedError;
  String? get thoughts => throw _privateConstructorUsedError;
  String? get emotion => throw _privateConstructorUsedError;
  int get intensity => throw _privateConstructorUsedError;
  String? get action => throw _privateConstructorUsedError;
  String? get result => throw _privateConstructorUsedError;
  String get created =>
      throw _privateConstructorUsedError; // 作成日時。ISO8601文字列表現（例：2019-04-30T01:48:27.701406Z）。
  String get updated => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $AngerLogCopyWith<AngerLog> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AngerLogCopyWith<$Res> {
  factory $AngerLogCopyWith(AngerLog value, $Res Function(AngerLog) then) =
      _$AngerLogCopyWithImpl<$Res>;
  $Res call(
      {int? id,
      String date,
      String time,
      String? event,
      String? thoughts,
      String? emotion,
      int intensity,
      String? action,
      String? result,
      String created,
      String updated});
}

/// @nodoc
class _$AngerLogCopyWithImpl<$Res> implements $AngerLogCopyWith<$Res> {
  _$AngerLogCopyWithImpl(this._value, this._then);

  final AngerLog _value;
  // ignore: unused_field
  final $Res Function(AngerLog) _then;

  @override
  $Res call({
    Object? id = freezed,
    Object? date = freezed,
    Object? time = freezed,
    Object? event = freezed,
    Object? thoughts = freezed,
    Object? emotion = freezed,
    Object? intensity = freezed,
    Object? action = freezed,
    Object? result = freezed,
    Object? created = freezed,
    Object? updated = freezed,
  }) {
    return _then(_value.copyWith(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      time: time == freezed
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      event: event == freezed
          ? _value.event
          : event // ignore: cast_nullable_to_non_nullable
              as String?,
      thoughts: thoughts == freezed
          ? _value.thoughts
          : thoughts // ignore: cast_nullable_to_non_nullable
              as String?,
      emotion: emotion == freezed
          ? _value.emotion
          : emotion // ignore: cast_nullable_to_non_nullable
              as String?,
      intensity: intensity == freezed
          ? _value.intensity
          : intensity // ignore: cast_nullable_to_non_nullable
              as int,
      action: action == freezed
          ? _value.action
          : action // ignore: cast_nullable_to_non_nullable
              as String?,
      result: result == freezed
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as String?,
      created: created == freezed
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as String,
      updated: updated == freezed
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$$_AngerLogCopyWith<$Res> implements $AngerLogCopyWith<$Res> {
  factory _$$_AngerLogCopyWith(
          _$_AngerLog value, $Res Function(_$_AngerLog) then) =
      __$$_AngerLogCopyWithImpl<$Res>;
  @override
  $Res call(
      {int? id,
      String date,
      String time,
      String? event,
      String? thoughts,
      String? emotion,
      int intensity,
      String? action,
      String? result,
      String created,
      String updated});
}

/// @nodoc
class __$$_AngerLogCopyWithImpl<$Res> extends _$AngerLogCopyWithImpl<$Res>
    implements _$$_AngerLogCopyWith<$Res> {
  __$$_AngerLogCopyWithImpl(
      _$_AngerLog _value, $Res Function(_$_AngerLog) _then)
      : super(_value, (v) => _then(v as _$_AngerLog));

  @override
  _$_AngerLog get _value => super._value as _$_AngerLog;

  @override
  $Res call({
    Object? id = freezed,
    Object? date = freezed,
    Object? time = freezed,
    Object? event = freezed,
    Object? thoughts = freezed,
    Object? emotion = freezed,
    Object? intensity = freezed,
    Object? action = freezed,
    Object? result = freezed,
    Object? created = freezed,
    Object? updated = freezed,
  }) {
    return _then(_$_AngerLog(
      id: id == freezed
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      date: date == freezed
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as String,
      time: time == freezed
          ? _value.time
          : time // ignore: cast_nullable_to_non_nullable
              as String,
      event: event == freezed
          ? _value.event
          : event // ignore: cast_nullable_to_non_nullable
              as String?,
      thoughts: thoughts == freezed
          ? _value.thoughts
          : thoughts // ignore: cast_nullable_to_non_nullable
              as String?,
      emotion: emotion == freezed
          ? _value.emotion
          : emotion // ignore: cast_nullable_to_non_nullable
              as String?,
      intensity: intensity == freezed
          ? _value.intensity
          : intensity // ignore: cast_nullable_to_non_nullable
              as int,
      action: action == freezed
          ? _value.action
          : action // ignore: cast_nullable_to_non_nullable
              as String?,
      result: result == freezed
          ? _value.result
          : result // ignore: cast_nullable_to_non_nullable
              as String?,
      created: created == freezed
          ? _value.created
          : created // ignore: cast_nullable_to_non_nullable
              as String,
      updated: updated == freezed
          ? _value.updated
          : updated // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_AngerLog with DiagnosticableTreeMixin implements _AngerLog {
  _$_AngerLog(
      {this.id,
      required this.date,
      required this.time,
      this.event,
      this.thoughts,
      this.emotion,
      required this.intensity,
      this.action,
      this.result,
      required this.created,
      required this.updated});

  factory _$_AngerLog.fromJson(Map<String, dynamic> json) =>
      _$$_AngerLogFromJson(json);

  @override
  final int? id;
  @override
  final String date;
  @override
  final String time;
  @override
  final String? event;
  @override
  final String? thoughts;
  @override
  final String? emotion;
  @override
  final int intensity;
  @override
  final String? action;
  @override
  final String? result;
  @override
  final String created;
// 作成日時。ISO8601文字列表現（例：2019-04-30T01:48:27.701406Z）。
  @override
  final String updated;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AngerLog(id: $id, date: $date, time: $time, event: $event, thoughts: $thoughts, emotion: $emotion, intensity: $intensity, action: $action, result: $result, created: $created, updated: $updated)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'AngerLog'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('date', date))
      ..add(DiagnosticsProperty('time', time))
      ..add(DiagnosticsProperty('event', event))
      ..add(DiagnosticsProperty('thoughts', thoughts))
      ..add(DiagnosticsProperty('emotion', emotion))
      ..add(DiagnosticsProperty('intensity', intensity))
      ..add(DiagnosticsProperty('action', action))
      ..add(DiagnosticsProperty('result', result))
      ..add(DiagnosticsProperty('created', created))
      ..add(DiagnosticsProperty('updated', updated));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AngerLog &&
            const DeepCollectionEquality().equals(other.id, id) &&
            const DeepCollectionEquality().equals(other.date, date) &&
            const DeepCollectionEquality().equals(other.time, time) &&
            const DeepCollectionEquality().equals(other.event, event) &&
            const DeepCollectionEquality().equals(other.thoughts, thoughts) &&
            const DeepCollectionEquality().equals(other.emotion, emotion) &&
            const DeepCollectionEquality().equals(other.intensity, intensity) &&
            const DeepCollectionEquality().equals(other.action, action) &&
            const DeepCollectionEquality().equals(other.result, result) &&
            const DeepCollectionEquality().equals(other.created, created) &&
            const DeepCollectionEquality().equals(other.updated, updated));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(id),
      const DeepCollectionEquality().hash(date),
      const DeepCollectionEquality().hash(time),
      const DeepCollectionEquality().hash(event),
      const DeepCollectionEquality().hash(thoughts),
      const DeepCollectionEquality().hash(emotion),
      const DeepCollectionEquality().hash(intensity),
      const DeepCollectionEquality().hash(action),
      const DeepCollectionEquality().hash(result),
      const DeepCollectionEquality().hash(created),
      const DeepCollectionEquality().hash(updated));

  @JsonKey(ignore: true)
  @override
  _$$_AngerLogCopyWith<_$_AngerLog> get copyWith =>
      __$$_AngerLogCopyWithImpl<_$_AngerLog>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_AngerLogToJson(this);
  }
}

abstract class _AngerLog implements AngerLog {
  factory _AngerLog(
      {final int? id,
      required final String date,
      required final String time,
      final String? event,
      final String? thoughts,
      final String? emotion,
      required final int intensity,
      final String? action,
      final String? result,
      required final String created,
      required final String updated}) = _$_AngerLog;

  factory _AngerLog.fromJson(Map<String, dynamic> json) = _$_AngerLog.fromJson;

  @override
  int? get id;
  @override
  String get date;
  @override
  String get time;
  @override
  String? get event;
  @override
  String? get thoughts;
  @override
  String? get emotion;
  @override
  int get intensity;
  @override
  String? get action;
  @override
  String? get result;
  @override
  String get created;
  @override // 作成日時。ISO8601文字列表現（例：2019-04-30T01:48:27.701406Z）。
  String get updated;
  @override
  @JsonKey(ignore: true)
  _$$_AngerLogCopyWith<_$_AngerLog> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$AngerLogList {
  bool get hasData => throw _privateConstructorUsedError;
  bool get hasNext => throw _privateConstructorUsedError;
  bool get isFetchingNext => throw _privateConstructorUsedError;
  int get offset => throw _privateConstructorUsedError;
  List<AngerLog> get list => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AngerLogListCopyWith<AngerLogList> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AngerLogListCopyWith<$Res> {
  factory $AngerLogListCopyWith(
          AngerLogList value, $Res Function(AngerLogList) then) =
      _$AngerLogListCopyWithImpl<$Res>;
  $Res call(
      {bool hasData,
      bool hasNext,
      bool isFetchingNext,
      int offset,
      List<AngerLog> list});
}

/// @nodoc
class _$AngerLogListCopyWithImpl<$Res> implements $AngerLogListCopyWith<$Res> {
  _$AngerLogListCopyWithImpl(this._value, this._then);

  final AngerLogList _value;
  // ignore: unused_field
  final $Res Function(AngerLogList) _then;

  @override
  $Res call({
    Object? hasData = freezed,
    Object? hasNext = freezed,
    Object? isFetchingNext = freezed,
    Object? offset = freezed,
    Object? list = freezed,
  }) {
    return _then(_value.copyWith(
      hasData: hasData == freezed
          ? _value.hasData
          : hasData // ignore: cast_nullable_to_non_nullable
              as bool,
      hasNext: hasNext == freezed
          ? _value.hasNext
          : hasNext // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetchingNext: isFetchingNext == freezed
          ? _value.isFetchingNext
          : isFetchingNext // ignore: cast_nullable_to_non_nullable
              as bool,
      offset: offset == freezed
          ? _value.offset
          : offset // ignore: cast_nullable_to_non_nullable
              as int,
      list: list == freezed
          ? _value.list
          : list // ignore: cast_nullable_to_non_nullable
              as List<AngerLog>,
    ));
  }
}

/// @nodoc
abstract class _$$_AngerLogListCopyWith<$Res>
    implements $AngerLogListCopyWith<$Res> {
  factory _$$_AngerLogListCopyWith(
          _$_AngerLogList value, $Res Function(_$_AngerLogList) then) =
      __$$_AngerLogListCopyWithImpl<$Res>;
  @override
  $Res call(
      {bool hasData,
      bool hasNext,
      bool isFetchingNext,
      int offset,
      List<AngerLog> list});
}

/// @nodoc
class __$$_AngerLogListCopyWithImpl<$Res>
    extends _$AngerLogListCopyWithImpl<$Res>
    implements _$$_AngerLogListCopyWith<$Res> {
  __$$_AngerLogListCopyWithImpl(
      _$_AngerLogList _value, $Res Function(_$_AngerLogList) _then)
      : super(_value, (v) => _then(v as _$_AngerLogList));

  @override
  _$_AngerLogList get _value => super._value as _$_AngerLogList;

  @override
  $Res call({
    Object? hasData = freezed,
    Object? hasNext = freezed,
    Object? isFetchingNext = freezed,
    Object? offset = freezed,
    Object? list = freezed,
  }) {
    return _then(_$_AngerLogList(
      hasData: hasData == freezed
          ? _value.hasData
          : hasData // ignore: cast_nullable_to_non_nullable
              as bool,
      hasNext: hasNext == freezed
          ? _value.hasNext
          : hasNext // ignore: cast_nullable_to_non_nullable
              as bool,
      isFetchingNext: isFetchingNext == freezed
          ? _value.isFetchingNext
          : isFetchingNext // ignore: cast_nullable_to_non_nullable
              as bool,
      offset: offset == freezed
          ? _value.offset
          : offset // ignore: cast_nullable_to_non_nullable
              as int,
      list: list == freezed
          ? _value._list
          : list // ignore: cast_nullable_to_non_nullable
              as List<AngerLog>,
    ));
  }
}

/// @nodoc

class _$_AngerLogList with DiagnosticableTreeMixin implements _AngerLogList {
  _$_AngerLogList(
      {required this.hasData,
      required this.hasNext,
      required this.isFetchingNext,
      required this.offset,
      required final List<AngerLog> list})
      : _list = list;

  @override
  final bool hasData;
  @override
  final bool hasNext;
  @override
  final bool isFetchingNext;
  @override
  final int offset;
  final List<AngerLog> _list;
  @override
  List<AngerLog> get list {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_list);
  }

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AngerLogList(hasData: $hasData, hasNext: $hasNext, isFetchingNext: $isFetchingNext, offset: $offset, list: $list)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'AngerLogList'))
      ..add(DiagnosticsProperty('hasData', hasData))
      ..add(DiagnosticsProperty('hasNext', hasNext))
      ..add(DiagnosticsProperty('isFetchingNext', isFetchingNext))
      ..add(DiagnosticsProperty('offset', offset))
      ..add(DiagnosticsProperty('list', list));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AngerLogList &&
            const DeepCollectionEquality().equals(other.hasData, hasData) &&
            const DeepCollectionEquality().equals(other.hasNext, hasNext) &&
            const DeepCollectionEquality()
                .equals(other.isFetchingNext, isFetchingNext) &&
            const DeepCollectionEquality().equals(other.offset, offset) &&
            const DeepCollectionEquality().equals(other._list, _list));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(hasData),
      const DeepCollectionEquality().hash(hasNext),
      const DeepCollectionEquality().hash(isFetchingNext),
      const DeepCollectionEquality().hash(offset),
      const DeepCollectionEquality().hash(_list));

  @JsonKey(ignore: true)
  @override
  _$$_AngerLogListCopyWith<_$_AngerLogList> get copyWith =>
      __$$_AngerLogListCopyWithImpl<_$_AngerLogList>(this, _$identity);
}

abstract class _AngerLogList implements AngerLogList {
  factory _AngerLogList(
      {required final bool hasData,
      required final bool hasNext,
      required final bool isFetchingNext,
      required final int offset,
      required final List<AngerLog> list}) = _$_AngerLogList;

  @override
  bool get hasData;
  @override
  bool get hasNext;
  @override
  bool get isFetchingNext;
  @override
  int get offset;
  @override
  List<AngerLog> get list;
  @override
  @JsonKey(ignore: true)
  _$$_AngerLogListCopyWith<_$_AngerLogList> get copyWith =>
      throw _privateConstructorUsedError;
}
