import 'dart:async';

import 'package:anger_log/db/db.dart';
import 'package:anger_log/exceptions/db_exception.dart';
import 'package:anger_log/models/models.dart';
import 'package:anger_log/common/error_code.dart';

class AngerLogDao {
  final dbProvider = DatabaseProvider();

  /// 新しい [AngerLog] を保存する。保存された [AngerLog.id] を返す。
  Future<int> createAngerLog(AngerLog angerLog) async {
    final db = await dbProvider.database;
    int result =
        await db.insert(dbProvider.tableName, angerLog.toJson()).catchError(
      (dynamic err) {
        throw const DBException(ErrorCode.dbInsertError);
      },
    );
    return result;
  }

  /// [List<AngerLog>] を [limit] 件ずつ取得して返す。
  /// [offset] の位置の次から取得開始する。
  Future<List<AngerLog>> getAngerLogs(int limit, int offset) async {
    final db = await dbProvider.database;
    List<Map<String, dynamic>> result = await db
        .query(dbProvider.tableName,
            orderBy: 'date desc, time desc, id desc', limit: limit, offset: offset)
        .catchError((dynamic err) {
      throw const DBException(ErrorCode.dbReadError);
    });
    List<AngerLog> angerLogs =
        result.map((item) => AngerLog.fromJson(item)).toList();
    return angerLogs;
  }

  /// [angerLog] を更新する。更新された行数を返す。
  Future<int> updateAngerLog(AngerLog angerLog) async {
    final db = await dbProvider.database;
    final List<int> whereArgs = [angerLog.id!];
    var result = await db
        .update(dbProvider.tableName, angerLog.toJson(),
            where: 'id = ?', whereArgs: whereArgs)
        .catchError(
      (dynamic err) {
        throw const DBException(ErrorCode.dbUpdateError);
      },
    );

    return result;
  }

  /// [id] の [angerLog] を消去する。削除された行数を返す。
  Future<int> deleteAngerLog(int id) async {
    final db = await dbProvider.database;
    return await db.delete(dbProvider.tableName,
        where: 'id = ?', whereArgs: [id]).catchError(
      (dynamic err) {
        throw const DBException(ErrorCode.dbDeleteError);
      },
    );
  }
}
