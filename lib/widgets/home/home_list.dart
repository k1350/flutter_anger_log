import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/models/models.dart';
import 'package:anger_log/pages/home.dart';
import 'package:anger_log/widgets/home/anger_log_card.dart';
import 'package:anger_log/widgets/error_dialog.dart';

class HomeList extends ConsumerStatefulWidget {
  const HomeList({Key? key}) : super(key: key);

  @override
  HomeListState createState() => HomeListState();
}

class HomeListState extends ConsumerState<HomeList> {
  final ScrollController _listScrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _getList(context);
    _listScrollController.addListener(_scrollListener);
  }

  Future<void> _scrollListener() async {
    final listState = ref.read(angerLogListProvider);
    if (listState.hasNext &&
        !listState.isFetchingNext &&
        _listScrollController.offset >=
            _listScrollController.position.maxScrollExtent &&
        !_listScrollController.position.outOfRange) {
      await ref.read(angerLogListProvider.notifier).get().catchError((err) {
        showDialog<int>(
          context: context,
          barrierDismissible: false,
          builder: (context) {
            return ErrorDialog(e: err);
          },
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final state = ref.watch(angerLogListProvider);
    if (state.hasData) {
      if (state.list.isNotEmpty) {
        return ListView(
          controller: _listScrollController,
          children: [
            for (AngerLog angerLog in state.list)
              AngerLogCard(angerLog: angerLog)
          ],
        );
      }
      return const Text('');
    }
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  Future<void> _getList(BuildContext context) async {
    await ref.read(angerLogListProvider.notifier).refresh().catchError((err) {
      showDialog<int>(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return ErrorDialog(e: err);
        },
      );
    });
  }
}
