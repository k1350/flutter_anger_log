import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

import 'package:anger_log/models/models.dart';
import 'package:anger_log/common/db_date_time.dart';
import 'package:anger_log/pages/add.dart';
import 'package:anger_log/pages/edit.dart';
import 'package:anger_log/pages/home.dart';
import 'package:anger_log/widgets/error_dialog.dart';

class AngerLogCard extends ConsumerWidget {
  const AngerLogCard({Key? key, required this.angerLog}) : super(key: key);

  final AngerLog angerLog;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final localDateTime =
        DbDateTime.getDateAndTime(angerLog.date, angerLog.time);
    final formatter = DateFormat('yyyy/MM/dd(E)', "ja_JP");
    return Material(
      child: InkWell(
        onTap: () async {
          ref.read(addPageProvider.notifier).setInitialState(
              id: angerLog.id,
              date: localDateTime.date,
              time: localDateTime.time,
              event: angerLog.event ?? '',
              thoughts: angerLog.thoughts ?? '',
              emotion: angerLog.emotion ?? '',
              intensity: angerLog.intensity,
              action: angerLog.action ?? '',
              result: angerLog.result ?? '',
              created: angerLog.created,
              updated: angerLog.updated);
          dynamic result = await Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => const EditPage()),
          );
          // 編集画面から戻った後、画面をリフレッシュする
          if (result != null) {
            await ref.read(angerLogListProvider.notifier).refresh().catchError((err) {
              showDialog<int>(
                context: context,
                barrierDismissible: false,
                builder: (context) {
                  return ErrorDialog(e: err);
                },
              );
            });
          }
        },
        child: Card(
          key: ObjectKey(angerLog.id),
          child: ListTile(
            title: Text(
                '${formatter.format(localDateTime.date)} ${localDateTime.time.format(context)}'),
            subtitle: Text('感情の強さ：${angerLog.intensity}'),
          ),
        ),
      ),
    );
  }
}
