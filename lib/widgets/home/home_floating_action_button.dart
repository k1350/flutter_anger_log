import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/pages/add.dart';
import 'package:anger_log/pages/home.dart';
import 'package:anger_log/widgets/error_dialog.dart';

class HomeFloatingActionButton extends ConsumerWidget {
  const HomeFloatingActionButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return FloatingActionButton(
      onPressed: () async {
        final now = DateTime.now();
        ref.read(addPageProvider.notifier).setInitialState(
            date: DateTime(now.year, now.month, now.day),
            time: TimeOfDay.now(),
            event: '',
            thoughts: '',
            emotion: '',
            intensity: 5,
            action: '',
            result: '');
        dynamic result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const AddPage()),
        );
        // 登録画面から戻った後、画面をリフレッシュする
        if (result != null) {
          await ref.read(angerLogListProvider.notifier).refresh().catchError((err) {
            showDialog<int>(
              context: context,
              barrierDismissible: false,
              builder: (context) {
                return ErrorDialog(e: err);
              },
            );
          });
        }
      },
      child: const Icon(Icons.add),
    );
  }
}
