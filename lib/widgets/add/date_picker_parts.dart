import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import "package:intl/intl.dart";

import 'package:anger_log/pages/add.dart';

class DatePickerParts extends ConsumerWidget {
  const DatePickerParts(
      {Key? key, required this.firstDate, required this.lastDate})
      : super(key: key);

  final DateTime firstDate;
  final DateTime lastDate;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final current = ref.watch(addPageProvider.select((state) {
      return state.date;
    }));
    final formatter = DateFormat('yyyy/MM/dd(E)', "ja_JP");
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(formatter.format(current)),
          DatePickerWidget(
              date: current, firstDate: firstDate, lastDate: lastDate),
        ],
      ),
    );
  }
}

class DatePickerWidget extends ConsumerStatefulWidget {
  const DatePickerWidget(
      {Key? key,
      required this.date,
      required this.firstDate,
      required this.lastDate})
      : super(key: key);

  final DateTime date;
  final DateTime firstDate;
  final DateTime lastDate;

  @override
  ConsumerState<DatePickerWidget> createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends ConsumerState<DatePickerWidget> {
  void onPressed() async {
    DateTime date = widget.date;
    final DateTime? picked = await showDatePicker(
        locale: const Locale('ja'),
        context: context,
        initialDate: date,
        firstDate: widget.firstDate,
        lastDate: widget.lastDate);
    ref.read(addPageProvider.notifier).setDate(picked ?? date);
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () => onPressed(),
      child: const Text('日付選択'),
    );
  }
}
