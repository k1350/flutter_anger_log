import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/pages/add.dart';

class EmotionParts extends ConsumerWidget {
  const EmotionParts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final current = ref.watch(addPageProvider.select((state) {
      return state.emotion;
    }));
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0),
      child: TextFormField(
        initialValue: current,
        decoration: const InputDecoration(labelText: '感情'),
        keyboardType: TextInputType.multiline,
        maxLines: null,
        textInputAction: TextInputAction.newline,
        onChanged: (value) {
          ref.read(addPageProvider.notifier).setEmotion(value);
        },
      ),
    );
  }
}
