import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/pages/add.dart';

class ActionParts extends ConsumerWidget {
  const ActionParts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final current = ref.watch(addPageProvider.select((state) {
      return state.action;
    }));
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0),
      child: TextFormField(
        initialValue: current,
        decoration: const InputDecoration(labelText: '行動'),
        keyboardType: TextInputType.multiline,
        maxLines: null,
        textInputAction: TextInputAction.newline,
        onChanged: (value) {
          ref.read(addPageProvider.notifier).setAction(value);
        },
      ),
    );
  }
}
