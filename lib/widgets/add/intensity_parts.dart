import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/pages/add.dart';

class IntensityParts extends ConsumerWidget {
  const IntensityParts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final intensity =
        ref.watch(addPageProvider.select((state) => state.intensity));
    return Padding(
        padding: const EdgeInsets.fromLTRB(10.0, 10.0, 30.0, 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            const Text('感情の強さ'),
            DropdownButton<String>(
                value: intensity.toString(),
                icon: const Icon(Icons.arrow_downward),
                onChanged: (value) {
                  if (value!.isNotEmpty) {
                    ref
                        .read(addPageProvider.notifier)
                        .setIntensity(int.parse(value));
                  }
                },
                items: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10']
                    .map<DropdownMenuItem<String>>((value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList()),
          ],
        ));
  }
}
