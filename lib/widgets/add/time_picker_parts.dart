import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/pages/add.dart';

class TimePickerParts extends ConsumerWidget {
  const TimePickerParts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final current = ref.watch(addPageProvider.select((state) {
      return state.time;
    }));
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(current.format(context)),
          TimePickerWidget(time: current),
        ],
      ),
    );
  }
}

class TimePickerWidget extends ConsumerStatefulWidget {
  const TimePickerWidget({Key? key, required this.time}) : super(key: key);

  final TimeOfDay time;

  @override
  ConsumerState<TimePickerWidget> createState() => _TimePickerWidgetState();
}

class _TimePickerWidgetState extends ConsumerState<TimePickerWidget> {
  void onPressed() async {
    TimeOfDay time = widget.time;
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: time,
    );
    ref.read(addPageProvider.notifier).setTime(picked ?? time);
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () => onPressed(),
      child: const Text('時刻選択'),
    );
  }
}
