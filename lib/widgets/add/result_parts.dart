import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/pages/add.dart';

class ResultParts extends ConsumerWidget {
  const ResultParts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final current = ref.watch(addPageProvider.select((state) {
      return state.result;
    }));
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
      child: TextFormField(
        initialValue: current,
        decoration: const InputDecoration(labelText: '結果'),
        keyboardType: TextInputType.multiline,
        maxLines: null,
        textInputAction: TextInputAction.newline,
        onChanged: (value) {
          ref.read(addPageProvider.notifier).setResult(value);
        },
      ),
    );
  }
}
