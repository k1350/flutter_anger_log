import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/pages/add.dart';

class ThoughtsParts extends ConsumerWidget {
  const ThoughtsParts({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final current = ref.watch(addPageProvider.select((state) {
      return state.thoughts;
    }));
    return Padding(
      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0),
      child: TextFormField(
        initialValue: current,
        decoration: const InputDecoration(labelText: '思ったこと'),
        keyboardType: TextInputType.multiline,
        maxLines: null,
        textInputAction: TextInputAction.newline,
        onChanged: (value) {
          ref.read(addPageProvider.notifier).setThoughts(value);
        },
      ),
    );
  }
}
