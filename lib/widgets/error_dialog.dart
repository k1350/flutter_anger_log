import 'package:flutter/material.dart';

import 'package:anger_log/exceptions/db_exception.dart';

class ErrorDialog extends StatelessWidget {
  final dynamic e;
  const ErrorDialog({Key? key, required this.e}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String ec = '';
    // アプリ側で用意した例外であればエラーコードを出す。
    if (e is DBException) {
      final DBException de = e as DBException;
      ec = '\n${de.message}';
    }
    return AlertDialog(
      title: const Text('エラー'),
      content: Text('エラーが発生しました$ec'),
      actions: <Widget>[
        ElevatedButton(
          child: Text(MaterialLocalizations.of(context).closeButtonLabel),
          onPressed: () => Navigator.of(context).pop(1),
        ),
      ],
    );
  }
}
