import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DbDateTimeResult {
  late String date;
  late String time;
  DbDateTimeResult({required this.date, required this.time});
}

class DateTimeResult {
  late DateTime date;
  late TimeOfDay time;
  DateTimeResult({required this.date, required this.time});
}

class DbDateTime {
  /// ローカルタイムゾーンで定義された [date], [time] から UTC の文字列を返す
  /// 返却する date は ISO8601String 形式
  /// 返却する time は hh:mm 形式
  static DbDateTimeResult getDbDateAndTime(DateTime date, TimeOfDay time) {
    final dt = DateTime(date.year, date.month, date.day, time.hour, time.minute)
        .toUtc();
    final utcDateString = DateTime(dt.year, dt.month, dt.day).toIso8601String();
    final utcTimeString =
        '${dt.hour.toString().padLeft(2, '0')}:${dt.minute.toString().padLeft(2, '0')}';
    return DbDateTimeResult(date: utcDateString, time: utcTimeString);
  }

  /// UTC で定義されたString 形式の [date], [time] から、ローカルタイムゾーンの [DateTime] オブジェクトと [TimeOfDay] オブジェクトを返す
  /// [date] は ISO8601String 形式
  /// [time] は hh:mm 形式
  static DateTimeResult getDateAndTime(String date, String time) {
    final parseDate = DateTime.parse(date);
    final t = DateFormat("hh:mm").parse(time);
    final utcTime = TimeOfDay.fromDateTime(t);
    parseDate.add(Duration(hours: utcTime.hour, minutes: utcTime.minute));
    final utcDateTime = DateTime.utc(parseDate.year, parseDate.month, parseDate.day, utcTime.hour, utcTime.minute);
    final localDateTime = utcDateTime.toLocal();

    final localDate =
        DateTime(localDateTime.year, localDateTime.month, localDateTime.day);
    final localTime =
        TimeOfDay(hour: localDateTime.hour, minute: localDateTime.minute);
    return DateTimeResult(date: localDate, time: localTime);
  }
}
