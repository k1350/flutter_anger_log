class ErrorCode {
  /// DBへの新規保存に失敗したエラー。
  static const String dbInsertError = "D-001";
  /// DBからの読み出しに失敗したエラー。
  static const String dbReadError = "D-002";
  /// DBの更新に失敗したエラー。
  static const String dbUpdateError = "D-003";
  /// DBからの削除に失敗したエラー。
  static const String dbDeleteError = "D-004";
}