import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/widgets/add/date_picker_parts.dart';
import 'package:anger_log/widgets/add/emotion_parts.dart';
import 'package:anger_log/widgets/add/result_parts.dart';
import 'package:anger_log/widgets/add/time_picker_parts.dart';
import 'package:anger_log/widgets/add/action_parts.dart';
import 'package:anger_log/widgets/add/event_parts.dart';
import 'package:anger_log/widgets/add/intensity_parts.dart';
import 'package:anger_log/widgets/add/thoughts_parts.dart';
import 'package:anger_log/widgets/error_dialog.dart';
import 'add.dart';

class EditPage extends ConsumerWidget {
  const EditPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('編集'),
          actions: [
            IconButton(
              icon: const Icon(Icons.delete_forever),
              onPressed: () {
                _delete(context, ref);
              },
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              DatePickerParts(
                  firstDate: DateTime(2022), lastDate: DateTime.now()),
              const TimePickerParts(),
              const EventParts(),
              const ThoughtsParts(),
              const EmotionParts(),
              const IntensityParts(),
              const ActionParts(),
              const ResultParts(),
              ElevatedButton(
                child: const Text('保存'),
                onPressed: () {
                  _update(context, ref);
                },
              ),
            ],
          ),
        ));
  }

  Future<void> _update(BuildContext context, WidgetRef ref) async {
    await ref.read(addPageProvider.notifier).update().then((value) {
      const snackBar = SnackBar(
        content: Text('保存しました'),
        backgroundColor: Colors.green,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      Navigator.pop(context, 'saved');
    }, onError: (err) {
      showDialog<int>(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return ErrorDialog(e: err);
        },
      );
    });
  }

  Future<void> _delete(BuildContext context, WidgetRef ref) async {
    await ref.read(addPageProvider.notifier).delete().then((value) {
      const snackBar = SnackBar(
        content: Text('削除しました'),
        backgroundColor: Colors.green,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      Navigator.pop(context, 'deleted');
    }, onError: (err) {
      showDialog<int>(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return ErrorDialog(e: err);
        },
      );
    });
  }
}
