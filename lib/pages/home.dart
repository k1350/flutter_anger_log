import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/widgets/home/home_floating_action_button.dart';
import 'package:anger_log/models/models.dart';
import 'package:anger_log/notifiers/anger_log_list_notifier.dart';
import 'package:anger_log/widgets/home/home_list.dart';

class TopPage extends StatelessWidget {
  const TopPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('アンガーログ')),
      body: const HomeList(),
      floatingActionButton: const HomeFloatingActionButton(),
    );
  }
}

final angerLogListProvider =
    StateNotifierProvider<AngerLogListNotifier, AngerLogList>((ref) {
  return AngerLogListNotifier();
});
