import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'package:anger_log/models/models.dart';
import 'package:anger_log/widgets/add/date_picker_parts.dart';
import 'package:anger_log/widgets/add/emotion_parts.dart';
import 'package:anger_log/widgets/add/result_parts.dart';
import 'package:anger_log/widgets/add/time_picker_parts.dart';
import 'package:anger_log/widgets/add/action_parts.dart';
import 'package:anger_log/widgets/add/event_parts.dart';
import 'package:anger_log/widgets/add/intensity_parts.dart';
import 'package:anger_log/widgets/add/thoughts_parts.dart';
import 'package:anger_log/notifiers/add_page_notifier.dart';
import 'package:anger_log/widgets/error_dialog.dart';

class AddPage extends ConsumerWidget {
  const AddPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
        appBar: AppBar(title: const Text('新規登録')),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              DatePickerParts(
                  firstDate: DateTime(2022), lastDate: DateTime.now()),
              const TimePickerParts(),
              const EventParts(),
              const ThoughtsParts(),
              const EmotionParts(),
              const IntensityParts(),
              const ActionParts(),
              const ResultParts(),
              ElevatedButton(
                child: const Text('保存'),
                onPressed: () async {
                  await _save(context, ref);
                },
              ),
            ],
          ),
        ));
  }

  Future<void> _save(BuildContext context, WidgetRef ref) async {
    await ref.read(addPageProvider.notifier).save().then((value) {
      const snackBar = SnackBar(
        content: Text('保存しました'),
        backgroundColor: Colors.green,
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      Navigator.pop(context, 'saved');
    }, onError: (err) {
      showDialog<int>(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return ErrorDialog(e: err);
        },
      );
    });
  }
}

final addPageProvider =
    StateNotifierProvider<AddPageStateNotifier, AddPageState>((ref) {
  return AddPageStateNotifier();
});
